package com.threatmodel.fstec.controller;

import com.threatmodel.fstec.model.Threat;
import com.threatmodel.fstec.repository.ThreatJpaRepository;
import com.threatmodel.fstec.service.ThreatParser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;

@Controller
@RequestMapping(value = "/threatcontrol")
public class ThreatController {
    @Autowired
    private ThreatJpaRepository threatJpaRepository;

    @Autowired
    private ThreatParser threatParser;

    @GetMapping(value = "/threats_update")
    public String redirectToThreatsUpdateForm() {
        return "threat_update_form";
    }

    @GetMapping(value = "/threatlist")
    public String threatList(Model model, @PageableDefault(value = 10) Pageable pageable) {
        Page<Threat> page = threatJpaRepository.findAll(pageable);
        model.addAttribute("threatList", page);
        return "threat_list";
    }

    @Transactional
    @PostMapping(value = "/update")
    public String updateThreatsList(Model model) throws IOException {
        if (!threatParser.isUpdateNeed()) {
            model.addAttribute("isUpdateNeed", "EVERYTHING IS UP TO DATE");
            return "threat_update_form";
        } else {
                threatParser.saveAllThreats();
            return "redirect:/threatcontrol/threats_update";
        }
    }

    @PostMapping(value = "/threats_update")
    public String checkUpdate(Model model) throws IOException {
        if (threatJpaRepository.findAll().size() == 0) {
            threatParser.isUpdateNeed();
            model.addAttribute("threatDifference", threatParser.findDifference(threatParser.threatsToUpdate));
            return "updater_form";
        } else {
            if (!threatParser.isUpdateNeed()) {
                model.addAttribute("isUpdateNeed", "EVERYTHING IS UP TO DATE");
                return "threat_update_form";
            } else {
                model.addAttribute("threatDifference", threatParser.findDifference(threatParser.threatsToUpdate));
                return "updater_form";
            }
        }
    }

    @GetMapping(value = "/updater")
    public String showUpdateForm() {
        return "updater_form";
    }

    @GetMapping(value = "/doupdate")
    public String updateThreats() {
        threatParser.updateFindedThreats();
        return "redirect:/threatcontrol/threats_update";
    }

    @ModelAttribute(value = "threatCount")
    public int getAll() {
        return threatParser.getThreatCount();
    }

    @GetMapping(value = "/{id}")
    public String showThreatInfo(@PathVariable("id") Integer id, Model model) {
        model.addAttribute("threatinfo", threatJpaRepository.findById(id).get());
        return "threat_info_form";
    }

    @GetMapping(value = "/deletethreat/{id}")
    public String deleteThreatById(@PathVariable("id") Integer id) {
        threatJpaRepository.deleteById(id);
        return "redirect:/threatcontrol/threatlist";
    }

    @Transactional
    @GetMapping(value = "/deleteall")
    public String deleteThreatById() {
        threatJpaRepository.deleteAll();
        return "redirect:/threatcontrol/threatlist";
    }
}

