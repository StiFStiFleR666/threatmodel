package com.threatmodel.fstec.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.io.IOException;

@Controller
@RequestMapping("/")
public class MainController {

    @GetMapping(value = "/")
    public String getMainPage() throws IOException {
        return "index";
    }
}

