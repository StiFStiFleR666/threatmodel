package com.threatmodel.fstec.service;

import com.threatmodel.fstec.model.ENUMS.IntrudersTypeEnum;
import com.threatmodel.fstec.model.Threat;
import com.threatmodel.fstec.repository.ThreatJpaRepository;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.*;
import java.net.URL;
import java.util.*;

@Service
public class ThreatParser {

    @Autowired
    private ThreatJpaRepository threatJpaRepository;

    public static ArrayList<Threat> threatsToUpdate = new ArrayList<>();

    private List<Threat> currentThreatList;

    private static void downloadUsingStream(String urlStr, String file) throws IOException {
        URL url = new URL(urlStr);
        BufferedInputStream bis = new BufferedInputStream(url.openStream());
        FileOutputStream fis = new FileOutputStream(file);
        byte[] buffer = new byte[1024];
        int count = 0;
        while ((count = bis.read(buffer, 0, 1024)) != -1) {
            fis.write(buffer, 0, count);
        }
        fis.close();
        bis.close();
    }

    public ArrayList<Threat> parseThreatsFromXML() throws IOException {

        String tempDir = System.getProperty("java.io.tmpdir");
        String file = tempDir + "/thrlist.xlsx";
        downloadUsingStream("https://bdu.fstec.ru/documents/files/thrlist.xlsx", file);

        InputStream inp = new FileInputStream(file);

        Workbook wb = new XSSFWorkbook(inp);
        Sheet dataSheet = wb.getSheetAt(0);
        Iterator<Row> iterator = dataSheet.iterator();

        ArrayList<String> resultList = new ArrayList<>();
        ArrayList<Threat> threatsList = new ArrayList<>();

        while (iterator.hasNext()) {
            String s = "";
            Row currentRow = iterator.next();
            Iterator<Cell> cellIterator = currentRow.iterator();

            while (cellIterator.hasNext()) {
                Cell currentCell = cellIterator.next();
                if (currentCell.getCellTypeEnum() == CellType.STRING) {
                    s = s + currentCell.getStringCellValue() + "--";
                } else if (currentCell.getCellTypeEnum() == CellType.NUMERIC) {
                    s = s + currentCell.getNumericCellValue() + "--";
                }
            }
            resultList.add(s);
        }

        for (int i = 2; i < resultList.size(); i++) {
            Threat threat = new Threat();
            String[] x = resultList.get(i).split("--");
            threat.setId(Integer.parseInt(x[0].substring(0, x[0].indexOf("."))));
            threat.setName(x[1].replaceAll("_x000d_", ""));
            threat.setDescription(x[2].replaceAll("_x000d_", ""));
            Set<IntrudersTypeEnum> intrudersSet = new HashSet<>();

            for (String s : x[3].split(",")) {
                if (s.contains("Внутренний") && s.contains("высоким")) {
                    intrudersSet.add(IntrudersTypeEnum.INNER_HIGH);
                }
                if (s.contains("Внутренний") && s.contains("средним")) {
                    intrudersSet.add(IntrudersTypeEnum.INNER_MID);
                }
                if (s.contains("Внутренний") && s.contains("низким")) {
                    intrudersSet.add(IntrudersTypeEnum.INNER_LOW);
                }
                if (s.contains("Внешний") && s.contains("высоким")) {
                    intrudersSet.add(IntrudersTypeEnum.OUTER_HIGH);
                }
                if (s.contains("Внешний") && s.contains("средним")) {
                    intrudersSet.add(IntrudersTypeEnum.OUTER_MID);
                }
                if (s.contains("Внешний") && s.contains("низким")) {
                    intrudersSet.add(IntrudersTypeEnum.OUTER_LOW);
                }
            }
            threat.setIntruders(intrudersSet);
            threat.setObjectsOfInfluence(x[4]);
            if (x[5].charAt(0) == '1') {
                threat.setConfidentiality(true);
            }
            if (x[6].charAt(0) == '1') {
                threat.setIntegrity(true);
            }
            if (x[7].charAt(0) == '1') {
                threat.setAvailability(true);
            }
            threatsList.add(threat);
        }
        return threatsList;
    }


    public boolean isUpdateNeed() throws IOException {
        threatsToUpdate = new ArrayList<>();
        currentThreatList = threatJpaRepository.findAll();
        ArrayList<Threat> newThreatList = this.parseThreatsFromXML();
        if (this.currentThreatList.equals(newThreatList)) {
            return false;
        } else {
            for (Threat threat : newThreatList) {
                if (!this.currentThreatList.contains(threat)) {
                    this.threatsToUpdate.add(threat);
                }
            }
            return true;
        }
    }

    public ArrayList<String> findDifference(ArrayList<Threat> threatsToUpdate) {
        ArrayList<String> differences = new ArrayList<>();
        for (Threat threat : threatsToUpdate) {
            try {
                Threat oldThreat = threatJpaRepository.findById(threat.getId()).get();
                String difference = "Differense is in Threat with ID:= " + threat.getId() + "!" + "\n";
                if (!(oldThreat.getName().equals(threat.getName()))) {
                    difference = difference + "current Name is {" + oldThreat.getName() + "}\n"
                            + "    new Name is {" + threat.getName() + "}";
                }
                if (!(oldThreat.getDescription().equals(threat.getDescription()))) {
                    difference = difference + "current Description is {" + oldThreat.getDescription() + "}\n"
                            + "    new Description is {" + threat.getDescription() + "}";
                }
                if (!(oldThreat.getObjectsOfInfluence().equals(threat.getObjectsOfInfluence()))) {
                    difference = difference + "current Objects Of Influence is {" + oldThreat.getObjectsOfInfluence() + "}\n"
                            + "    new Objects Of Influence is {" + threat.getObjectsOfInfluence() + "}";
                }
                if (!(oldThreat.getIntruders().equals(threat.getIntruders()))) {
                    difference = difference + "current Intruders is {" + oldThreat.getIntruders() + "}\n"
                            + "    new Intruders is {" + threat.getIntruders() + "}";
                }
                if (!(oldThreat.isConfidentiality() == (threat.isConfidentiality()))) {
                    difference = difference + "current Confidentiality is " + oldThreat.isConfidentiality() + "\n"
                            + "    new Confidentiality is {" + threat.isConfidentiality() + "}";
                }
                if (!(oldThreat.isIntegrity() == (threat.isIntegrity()))) {
                    difference = difference + "current Integrity is {" + oldThreat.isIntegrity() + "}\n"
                            + "    new Integrity is {" + threat.isIntegrity() + "}";
                }
                if (!(oldThreat.isAvailability() == (threat.isAvailability()))) {
                    difference = difference + "current Availability is {" + oldThreat.isAvailability() + "}\n"
                            + "    new Availability is {" + threat.isAvailability() + "}";
                }
                differences.add(difference);
            } catch (Exception e) {
                differences.add("НОВАЯ УГРОЗА: " + threat.toString());
            }
        }
        return differences;
    }

    @Transactional
    public void updateFindedThreats() {
        for (Threat threat : this.threatsToUpdate) {
            threatJpaRepository.save(threat);
        }
    }

    public int getThreatCount() {
        return threatJpaRepository.findAll().size();
    }

    public void saveAllThreats() throws IOException {
        threatJpaRepository.saveAll(parseThreatsFromXML());
    }
}
