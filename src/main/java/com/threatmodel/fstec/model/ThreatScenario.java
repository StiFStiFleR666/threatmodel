package com.threatmodel.fstec.model;

import com.threatmodel.fstec.model.ENUMS.MethodsOfImplementationEnum;
import com.threatmodel.fstec.model.ENUMS.ProbabilityLevelEnum;

import java.util.ArrayList;

public class ThreatScenario {
    private int id;
    private Threat threat;
    private Intruder intruder;
    private ProbabilityLevelEnum probability;
    private boolean relevance;
    private ArrayList<MethodsOfImplementationEnum> methods;
}
