package com.threatmodel.fstec.model;

import com.threatmodel.fstec.model.ENUMS.DangerLevelEnum;

import java.util.HashMap;

public class IntruderModel {
    private int id;
    private HashMap<Intruder, DangerLevelEnum> intrudersList;
    private Organization organization;
}
