package com.threatmodel.fstec.model;

import com.threatmodel.fstec.model.ENUMS.DangerLevelEnum;
import com.threatmodel.fstec.model.ENUMS.IntrudersTypeEnum;

public class Intruder {
    private int id;
    private String name;
    private Organization organization;
    private String description;
    private IntrudersTypeEnum type;
    private DangerLevelEnum dangerLevel;
}
