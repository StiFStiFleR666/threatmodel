package com.threatmodel.fstec.model;


import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.threatmodel.fstec.model.ENUMS.IntrudersTypeEnum;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Objects;
import java.util.Set;

@Entity
@Table(name = "Threats")
@JsonAutoDetect
public class Threat {
    @Id
    private int id;

    @NotNull
    @Column(length = 255)
    private String name;

    @NotNull
    @Column(length = 2000)
    private String description;

    @ElementCollection(targetClass = IntrudersTypeEnum.class)
    @CollectionTable(
            name = "threat_intruders",
            joinColumns = {@JoinColumn(name = "intruders")}
    )
    @Enumerated(EnumType.STRING)
    @Column(name = "intruders_id")
    private Set<IntrudersTypeEnum> intruders;

    @NotNull
    @Column(length = 500)
    private String objectsOfInfluence;

    @NotNull
    private boolean confidentiality;

    @NotNull
    private boolean integrity;

    @NotNull
    private boolean availability;

    public Threat() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Set<IntrudersTypeEnum> getIntruders() {
        return intruders;
    }

    public void setIntruders(Set<IntrudersTypeEnum> intruders) {
        this.intruders = intruders;
    }

    public String getObjectsOfInfluence() {
        return objectsOfInfluence;
    }

    public void setObjectsOfInfluence(String objectsOfInfluence) {
        this.objectsOfInfluence = objectsOfInfluence;
    }

    public boolean isConfidentiality() {
        return confidentiality;
    }

    public void setConfidentiality(boolean confidentiality) {
        this.confidentiality = confidentiality;
    }

    public boolean isIntegrity() {
        return integrity;
    }

    public void setIntegrity(boolean integrity) {
        this.integrity = integrity;
    }

    public boolean isAvailability() {
        return availability;
    }

    public void setAvailability(boolean availability) {
        this.availability = availability;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Threat)) return false;
        Threat threat = (Threat) o;
        return getId() == threat.getId() &&
                isConfidentiality() == threat.isConfidentiality() &&
                isIntegrity() == threat.isIntegrity() &&
                isAvailability() == threat.isAvailability() &&
                Objects.equals(getName(), threat.getName()) &&
                Objects.equals(getDescription(), threat.getDescription()) &&
                Objects.equals(getIntruders(), threat.getIntruders()) &&
                Objects.equals(getObjectsOfInfluence(), threat.getObjectsOfInfluence());
    }

    @Override
    public int hashCode() {

        return Objects.hash(getId(), getName(), getDescription(), getIntruders(), getObjectsOfInfluence(), isConfidentiality(), isIntegrity(), isAvailability());
    }

    @Override
    public String toString() {
        return "Threat{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", intruders=" + intruders +
                ", objectsOfInfluence='" + objectsOfInfluence + '\'' +
                ", confidentiality=" + confidentiality +
                ", integrity=" + integrity +
                ", availability=" + availability +
                '}';
    }
}
