package com.threatmodel.fstec.model.ENUMS;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "objects")
public class ObjectsOfInfluenceEnum {
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Id
    private Integer id;

    private String name;

    private boolean correct = false;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "ObjectsOfInfluenceEnum{" +
                "name='" + name + '\'' +
                '}';
    }
}
