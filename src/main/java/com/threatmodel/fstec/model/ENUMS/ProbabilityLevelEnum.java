package com.threatmodel.fstec.model.ENUMS;

public enum  ProbabilityLevelEnum {
    ZERO,
    LOW,
    MEDIUM,
    HIGHT
}
