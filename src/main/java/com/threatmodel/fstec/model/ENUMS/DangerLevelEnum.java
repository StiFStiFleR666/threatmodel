package com.threatmodel.fstec.model.ENUMS;

public enum DangerLevelEnum {
    HIGH,
    MEDIUM,
    LOW
}
