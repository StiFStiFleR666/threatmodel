package com.threatmodel.fstec.model.ENUMS;

public enum IntrudersTypeEnum {
    INNER_HIGH,
    INNER_MID,
    INNER_LOW,
    OUTER_HIGH,
    OUTER_MID,
    OUTER_LOW
}
