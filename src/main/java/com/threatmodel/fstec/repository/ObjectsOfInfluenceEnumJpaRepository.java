package com.threatmodel.fstec.repository;

import com.threatmodel.fstec.model.ENUMS.ObjectsOfInfluenceEnum;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ObjectsOfInfluenceEnumJpaRepository extends JpaRepository<ObjectsOfInfluenceEnum, Integer> {
}
