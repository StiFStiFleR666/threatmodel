package com.threatmodel.fstec.repository;


import com.threatmodel.fstec.model.Threat;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ThreatJpaRepository extends JpaRepository<Threat, Integer> {
}

