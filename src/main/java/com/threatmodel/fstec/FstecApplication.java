package com.threatmodel.fstec;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FstecApplication {

    public static void main(String[] args) {
        SpringApplication.run(FstecApplication.class, args);
    }
}
